package com.thoughtworks.springbootemployee.controller;

import com.thoughtworks.springbootemployee.model.Company;
import com.thoughtworks.springbootemployee.model.Employee;
import com.thoughtworks.springbootemployee.repository.CompanyRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/companies")
public class CompanyController {
    private static final CompanyRepository companyRepository = new CompanyRepository();
    @GetMapping
    public ResponseEntity<List<Company>> getAll(){
        return new ResponseEntity<>(companyRepository.getAll(), HttpStatus.OK);
    }
    @GetMapping("/{companyId}")
    public ResponseEntity<Company> getCompany(@PathVariable Long companyId){
        return new ResponseEntity<>(companyRepository.getCompanyById(companyId), HttpStatus.OK);
    }
    @GetMapping("/{companyId}/employees")
    public ResponseEntity<List<Employee>> getEmployeeListByCompanyId(@PathVariable Long companyId){
        return new ResponseEntity<>( companyRepository.getEployeeList(companyId), HttpStatus.OK);
    }
    @GetMapping("/page")
    public ResponseEntity<List<Company>> getPage(@RequestParam int page,@RequestParam int size){
        return new ResponseEntity<>(companyRepository.getCompanyListByPage(page, size), HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<Company> create(@RequestBody Company company) {
        return new ResponseEntity<>(companyRepository.insert(company), HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Company> update(@PathVariable Long id, @RequestBody Company company) {
        return new ResponseEntity<>(companyRepository.updateById(id,company), HttpStatus.OK);
    }

    @DeleteMapping("{id}")
    public ResponseEntity<Company> delete(@PathVariable Long id) {
        return new ResponseEntity<>(companyRepository.deleteById(id),HttpStatus.NO_CONTENT);
    }

}
