package com.thoughtworks.springbootemployee.controller;

import com.thoughtworks.springbootemployee.model.Employee;
import com.thoughtworks.springbootemployee.repository.EmployeeRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/employees")
public class EmployeeController {
   public static final EmployeeRepository employeeRepository = new EmployeeRepository();

    @PostMapping
    public ResponseEntity<Employee> createEmployee(@RequestBody Employee employee){

        return new ResponseEntity<Employee>(employeeRepository.insert(employee), HttpStatus.CREATED);
    }
    @GetMapping
    public ResponseEntity<List<Employee>> getEmployees(){
        return new ResponseEntity<List<Employee>>(employeeRepository.getAll(), HttpStatus.OK);
    }
    @GetMapping("/{employeeId}")
    public ResponseEntity<Employee> getEmployee(@PathVariable Long employeeId){
        return new ResponseEntity<Employee>(employeeRepository.getEmployeeById(employeeId), HttpStatus.OK);
    }
    @GetMapping(params = {"gender"})
    public ResponseEntity<List<Employee>> getEmployeeListByGender(@RequestParam String gender){
        return new ResponseEntity<List<Employee>>(employeeRepository.getEmployeeListByGender(gender), HttpStatus.OK);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Employee> update(@PathVariable Long id,@RequestBody Employee employee){
        return new ResponseEntity<>(employeeRepository.updateEmployeeById(id,employee), HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> delete(@PathVariable Long id){
        employeeRepository.deleteById(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @GetMapping("/page")
    public ResponseEntity<List<Employee>> getPage(@RequestParam int page,@RequestParam int size){
        return new ResponseEntity<>(employeeRepository.getEmployeeListByPage(page, size), HttpStatus.OK);
    }



}
