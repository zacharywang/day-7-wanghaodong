package com.thoughtworks.springbootemployee.repository;

import com.thoughtworks.springbootemployee.model.Employee;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;

public class EmployeeRepository {
    private List<Employee> employeeList = new ArrayList<>();
    private static AtomicLong atomicLongId = new AtomicLong(0);

    public Employee insert(Employee employee) {
        employee.setId(atomicLongId.incrementAndGet());
        employeeList.add(employee);
        return employee;
    }

    public List<Employee> getAll() {
        return employeeList;
    }

    public Employee getEmployeeById(Long employeeId) {
        for (Employee employee : employeeList) {
            if (employee.getId() == employeeId) {
                return employee;
            }
        }
        return new Employee();
    }

    public List<Employee> getEmployeeListByGender(String gender) {
        return employeeList.stream()
                .filter(employee -> employee.getGender().equals(gender))
                .collect(Collectors.toList());
    }

    public Employee updateEmployeeById(Long id, Employee employee) {
        employeeList.stream()
                .filter(employee1 -> id == employee1.getId())
                .forEach(employee1 -> {
                    employee1.setAge(employee.getAge());
                    employee1.setSalary(employee.getSalary());
                });
        return employee;
    }
    public void deleteById(Long id){
        employeeList.removeIf(employee -> employee.getId() == id);
    }
    public List<Employee> getEmployeeListByPage(int page, int size) {
        return employeeList.stream()
                .skip((long) (page - 1) * size)
                .limit(size)
                .collect(Collectors.toList());

    }
    public List<Employee> getEmployeeListByCompanyId(Long companyId) {
        return employeeList.stream()
                .filter(employee -> employee.getCompanyId().equals(companyId))
                .collect(Collectors.toList());
    }
}
