package com.thoughtworks.springbootemployee.repository;

import com.thoughtworks.springbootemployee.controller.EmployeeController;
import com.thoughtworks.springbootemployee.model.Company;
import com.thoughtworks.springbootemployee.model.Employee;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;

public class CompanyRepository {
    private List<Company> companies = new ArrayList<>();
    private static AtomicLong atomicLongId = new AtomicLong(0);
    public Company insert(Company company) {
        company.setId(atomicLongId.incrementAndGet());
        companies.add(company);
        return company;
    }

    public List<Company> getAll() {
        return companies;
    }

    public Company getCompanyById(Long companyId) {
        return companies.stream().filter(company -> Objects.equals(company.getId(),companyId)).findFirst().orElse(new Company());
    }
    public Company updateById(Long id, Company sourceCompany) {
        for (Company company : companies) {
            if (Objects.equals(company.getId(), id)) {
                company.setName(company.getName());
                return company;
            }
        }
        return sourceCompany;
    }

    public Company deleteById(Long id) {
        for (Company company : companies) {
            if (Objects.equals(company.getId(), id)) {
                companies.remove(company);
                return company;
            }
        }
        return null;
    }

    public List<Employee> getEployeeList(Long companyId){
       return EmployeeController.employeeRepository.getEmployeeListByCompanyId(companyId);
    }

    public List<Company> getCompanyListByPage(int page, int size) {
        return companies.stream()
                .skip((long) (page - 1) * size)
                .limit(size)
                .collect(Collectors.toList());

    }

}
